//import "meteor/blaze";
//import { Tracker } from "meteor/tracker";

if (!Tracker) {
  Tracker = {};
}

// Only trigger enclosing computation if the return value of
// f is not EJSON.equals to the previuous return value
if (!Tracker.guard) {
  Tracker.guard = function guard(f) {
    if (Meteor.isServer || !Tracker.currentComputation) {
      return f();
    }

    const dep = new Tracker.Dependency();


    const curView = Blaze.currentView;


    const tplFunc = Template._currentTemplateInstanceFunc;
    dep.depend();

    let value;
    let newValue;
    const autorunner = Tracker;
    autorunner.autorun((comp) => {
      if (curView) {
        newValue = Blaze._withCurrentView(curView, () => (tplFunc ? Template._withTemplateInstanceFunc(tplFunc, f) : f()));
      }
      else {
        newValue = f();
      }
      if (!comp.firstRun && !EJSON.equals(newValue, value)) {
        dep.changed();
      }
      value = EJSON.clone(newValue);
    });

    return newValue;
  };

  // Return a function which will always return the same
  // value as f and changes value whenever f changes value,
  // but is more efficient because it caches the value of f
  // in a reactive variable.
  Tracker.memo = function memo(f) {
    if (Meteor.isServer) return f;

    const value = new ReactiveVar(undefined, EJSON.equals);


    const tracker = Tracker.nonreactive(() => Template.instance()) || Tracker;


    let firstRun = true;


    const comp = tracker.autorun(() => {
      if (!firstRun) value.set(f());
    });

    return () => {
      if (firstRun) {
        firstRun = false;
        comp._compute();
      }

      return value.get();
    };
  };

  // Starting work on tracking who triggered an autorun
  /*
  const originalCompute = Tracker.Computation.prototype._compute;
  Tracker.Computation.prototype._compute = function _compute() {
    originalCompute.call(this);
    this.callers = [];
  };
  */
/*
  const originalInvalidate = Tracker.Computation.prototype.invalidate;
  Tracker.Computation.prototype.invalidate = function invalidate() {
    if (!this._callers) this._callers = [];

    const e = new Error("dummy");
    const stack = e.stack.replace(/^[^\(]+?[\n$]/gm, "")
    .replace(/^\s+at\s+/gm, "")
    .replace(/^Object.<anonymous>\s*\(/gm, "{anonymous}()@")
    .split("\n");
    this._callers.push(stack);

    originalInvalidate.call(this);
  };*/
  function get(obj, fParts) {
    while (obj && fParts.length) {
      obj = obj[fParts.splice(0, 1)[0]];
      if (obj instanceof Function && fParts.length) {
        obj = obj();
      }
    }
    return obj;
  }
  Template.partialData = function partialData(...fields) {
    if (Array.isArray(fields) && fields.length === 1 && Array.isArray(fields[0])) {
      fields = fields[0];
    }
    return Tracker.guard(() => {
      const data = (this && this.view) ? Blaze.getData(this.view) : Template.currentData();
      const ret = {};
      fields.forEach((f) => {
        const fParts = f.split('.');
        if (fParts.length === 0) {
          ret[f] = data[f];
        }
        else {
          ret[f] = get(data, fParts);
        }
      });
      return ret;
    });
  };
};

if (!Tracker.once) {
  /**
    @param fnPrecondition function to re-enable reactivity (optional)
    @param fnBody the body to be called
    @param useGuard should Tracker.guard wrap the fnPrecondition? (optional)
    @param onError standard onError argument to autorun (optional)
    @this could be a templateInstance, or the Tracker class.

    @description Run the fnBody reactively until comp.pause() is called.
    At which point fnBody will not be called again.
    If comp.resume() is called, reactivity is resumed.
    If comp.resume(true) is called, reactivity is resumed, and fnBody is immediately called.
    If fnPrecondition is provided, reactivity in it will trigger comp.resume(true)
    The result of fnPrecondition is passed to fnBody as the second argument
    if useGuard == true, pfnPrecondition will be wrapped in Tracker.guard
  */
  Tracker.once = function once(fnPrecondition, fnBody, { onError, useGuard = false } = {}) {
    if (!fnBody) {
      fnBody = fnPrecondition;
      fnPrecondition = null;
    }
    else if (!_.isFunction(fnBody)) {
      onError = fnBody.onError;
      useGuard = fnBody.useGuard;
      fnBody = fnPrecondition;
      fnPrecondition = null;
    }
    if (!fnBody) {
      throw new Meteor.Error("Expected at least one function as an argument");
    }
    const autorun = this.autorun || Tracker.autorun;
    const result = new ReactiveVar();
    const paused = new ReactiveVar(false);
    const forceResume = new Tracker.Dependency();
    useGuard = Tracker.guard && useGuard;
    let preconditionResult;
    if (fnPrecondition) {
      autorun.call(this, (comp) => {
        comp.result = result;
        comp.forceResume = forceResume;
        comp.paused = paused;
        preconditionResult = useGuard ? Tracker.guard(() => fnPrecondition(comp)) : fnPrecondition(comp);
        comp.resume(true);
      }, { onError });
    }
    return autorun.call(this, (comp) => {
      comp.result = result;
      comp.forceResume = forceResume;
      comp.paused = paused;
      forceResume.depend();
      if (!Tracker.nonreactive(() => paused.get())) {
        fnBody(comp, preconditionResult);
      }
    }, { onError });
  };

  Blaze.TemplateInstance.prototype.once = Tracker.once;

  Tracker.Computation.prototype.pause = function pause(result) {
    this.result.set(result);
    this.paused.set(true);
  };


  Tracker.Computation.prototype.resume = function resume(force) {
    this.paused.set(false);
    if (force) {
      this.forceResume.changed();
    }
  };
}
