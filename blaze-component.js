import "./tracker.js";
import _ from "underscore";
import { ReactiveDict } from "meteor/reactive-dict";


// NOTE: these two functions are used by a custom version of Blaze._getTemplateHelper
function bindDataContext(x) {
  if (typeof x === "function") {
    return function bindDataContextInner(...args) {
      let data = x.useNonReactiveData ? Tracker.nonreactive(() => Blaze.getData()) : Blaze.getData();
      if (data == null) data = {};
      return x.apply(data, args);
    };
  }
  return x;
}

function wrapHelper(f, templateFunc) {
  if (typeof f !== "function") {
    return f;
  }

  return function wrapHelperInner(...args) {
    const self = this;
    return Blaze.Template._withTemplateInstanceFunc(
      templateFunc,
      () => Blaze._wrapCatchingExceptions(f, "template helper").apply(self, args)
    );
  };
}


Blaze._getGlobalHelper = function (name, tmplInstanceFunc) {
  if (Blaze._globalHelpers[name] != null) {
    return wrapHelper(bindDataContext(Blaze._globalHelpers[name]), tmplInstanceFunc);
  }
  return null;
};

// NOTE: this version of Blaze._getTemplateHelper is used to allow selective reactivity of blaze helpers.
Blaze._getTemplateHelper = function _getTemplateHelper(template, name, tmplInstanceFunc) {
  // XXX COMPAT WITH 0.9.3
  let isKnownOldStyleHelper = false;

  if (template.__helpers.has(name)) {
    const helper = template.__helpers.get(name);
    if (helper === Blaze._OLDSTYLE_HELPER) {
      isKnownOldStyleHelper = true;
    }
    else if (helper != null) {
      try {
        const tmplInstance = Tracker.nonreactive(() => tmplInstanceFunc());
        if (tmplInstance && tmplInstance.useNonReactiveDataForHelpers === true && helper.useNonReactiveData === undefined) {
          helper.useNonReactiveData = true;
        }
      }
      catch (e) {
        console.warn(e);
      }
      return wrapHelper(bindDataContext(helper), tmplInstanceFunc);
    }
    else {
      return null;
    }
  }

  // old-style helper
  if (name in template) {
    // Only warn once per helper
    if (!isKnownOldStyleHelper) {
      template.__helpers.set(name, Blaze._OLDSTYLE_HELPER);
      if (!template._NOWARN_OLDSTYLE_HELPERS) {
        Blaze._warn(`Assigning helper with \`${template.viewName}.${
          name} = ...\` is deprecated.  Use \`${template.viewName
        }.helpers(...)\` instead.`);
      }
    }
    if (template[name] != null) {
      return wrapHelper(bindDataContext(template[name]), tmplInstanceFunc);
    }
  }

  return null;
};

export class BlazeComponent {
  constructor(templateInstance, settings = {}) {
    this.templateInstance = templateInstance;
    this._settings = new ReactiveDict(settings);
    this._useReactiveVars = new ReactiveDict();
    this._reactiveVars = new Map();
    this.timeouts = [];
    this.intervals = [];
    this.listeners = [];
    if (this.init) {
      this.init();
    }
  }

  bindDataToState(...fields) {
    if (fields.length === 1 && fields[0] instanceof Array) {
      fields = fields[0];
    }
    let dataKeys = fields;
    let stateKeys = fields;
    if (fields.length === 1 && fields[0] instanceof Object) {
      dataKeys = Object.keys(fields[0]);
      stateKeys = Object.values(fields[0]);
    }
    dataKeys.forEach((dataKey, index) => {
      this.dataChangedStrict(dataKey, (data) => this.set(stateKeys[index], data[dataKey]));
    });
  }

  nonReactiveData() {
    return this.templateInstance.data;
  }

  reactiveData(...fields) {
    if (fields.length) {
      return Template.partialData.call(this.templateInstance, ...fields);
    }
    // Template.currentData is the current data of the template, not the view (e.g., not the root level)
    return Tracker.guard(() => {
      return (this.templateInstance && this.templateInstance.view) ? Blaze.getData(this.templateInstance.view) : Template.currentData();
    });
  }

  once(...args) {
    if (!Tracker.once) {
      throw new Meteor.Error("You don't have Tracker.once");
    }
    return Tracker.once.call(this, ...args);
  }

  autorun(...args) {
    return this.templateInstance.autorun(...args);
  }

  subscribe(...args) {
    return this.templateInstance.subscribe(...args);
  }

  get settings() {
    return this._settings;
  }

  _stateChanged(fields = [], callback = () => {}, strict = false) {
    return this.autorun((computation) => {
      let state;
      if (fields.length === 0) {
        state = this._settings.all();
      }
      else {
        state = _.object(fields, fields.map(f => this.get(f)));
      }
      if (!strict) {
        callback.call(this, state, computation);
      }
      else {
        Tracker.nonreactive(() => callback.call(this, state, computation));
      }
    });
  }

  stateChanged(...fields) {
    const callback = fields.splice(fields.length - 1, 1)[0];
    return this._stateChanged(fields, callback, false);
  }

  // @deprecated
  statChangedStrict(...fields) {
    const callback = fields.splice(fields.length - 1, 1)[0];
    return this._stateChanged(fields, callback, true);
  }

  stateChangedStrict(...fields) {
    const callback = fields.splice(fields.length - 1, 1)[0];
    return this._stateChanged(fields, callback, true);
  }

  _dataChanged(fields = [], callback = () => {}, strict = false) {
    return this.autorun((computation) => {
      const data = fields.length === 0 ? Template.currentData() : Template.partialData(...fields);
      if (!strict) {
        callback.call(this, data, computation);
      }
      else {
        Tracker.nonreactive(() => callback.call(this, data, computation));
      }
    });
  }

  dataChanged(...fields) {
    const callback = fields.splice(fields.length - 1, 1)[0];
    return this._dataChanged(fields, callback, false);
  }

  dataChangedStrict(...fields) {
    const callback = fields.splice(fields.length - 1, 1)[0];
    return this._dataChanged(fields, callback, true);
  }

  set(settingName, value, useReactiveVar = false) {
    const existingUse = Tracker.nonreactive(() => this._useReactiveVars.get(settingName));
    if (existingUse !== useReactiveVar) {
      this._useReactiveVars.set(settingName, useReactiveVar);
    }
    if (useReactiveVar) {
      if (!this._reactiveVars.has(settingName)) {
        this._reactiveVars.set(settingName, new ReactiveVar(value));
      }
      else {
        this._reactiveVars.get(settingName).set(value);
      }
    }
    else {
      this._settings.set(settingName, value);
    }
  }

  get(settingName) {
    const useReactiveVar = this._useReactiveVars.set(settingName);
    if (useReactiveVar) {
      if (!this._reactiveVars.has(settingName)) {
        this._reactiveVars.set(settingName, new ReactiveVar());
      }
      return this._reactiveVars.get(settingName).get();
    }
    else {
      return this._settings.get(settingName);
    }
  }

  setTimeout(callback, time, { name } = {}) {
    if (name) {
      const oldEntry = this.timeouts.find(i => i.name === name);
      if (oldEntry) {
        this.clearInterval(oldEntry.id);
        this.timeouts.splice(this.timeouts.indexOf(oldEntry), 1);
      }
    }
    const id = Meteor.setTimeout(callback, time);
    this.timeouts.push({
      id,
      name
    });
    return id;
  }

  clearTimeout(id) {
    Meteor.clearTimeout(id);
    this.timeouts.splice(this.timeouts.indexOf(id), 1);
  }

  setInterval(callback, interval, { name } = {}) {
    if (name) {
      const oldEntry = this.intervals.find(i => i.name === name);
      if (oldEntry) {
        this.clearInterval(oldEntry.id);
        this.intervals.splice(this.intervals.indexOf(oldEntry), 1);
      }
    }
    const id = Meteor.setInterval(callback, interval);
    this.intervals.push({
      id,
      name
    });
    return id;
  }

  clearInterval(id) {
    Meteor.clearInterval(id);
    this.intervals.splice(this.intervals.indexOf(id), 1);
  }

  $(...args) {
    return this.templateInstance.$(...args);
  }

  on(elementOrSelector, event, f) {
    this.listeners.push({
      event,
      elementOrSelector,
      f
    });
    $(elementOrSelector).on(event, f);
  }


  destructor() {
    this.intervals.forEach(entry => Meteor.clearInterval(entry.id));
    this.timeouts.forEach(entry => Meteor.clearTimeout(entry.id));
    this.listeners.forEach(entry => $(entry.elementOrSelector).off(entry.event, entry.f));
  }
}

function registerHelpers(template, Klass, helperNames, fnNames) {
  if (helperNames.length) {
    template.helpers(_.object(helperNames, fnNames.map((handlerName) => {
      function helper(...args) {
        const componentInstance = Tracker.nonreactive(() => Template.instance())._component;
        return componentInstance[handlerName].call(componentInstance, ...args);
      }
      helper.useNonReactiveData = Klass.prototype[handlerName] && Klass.prototype[handlerName].useNonReactiveData;
      return helper;
    })));
  }
}

BlazeComponent.register = function register(template, Klass) {
  const events = (Klass.EventMap ? Klass.EventMap() : {}) || {};
  if (Object.keys(events).length) {
    template.events(_.object(_.keys(events), _.map(events, handlerName => function eventHandler(event, templateInstance, ...extra) {
      const componentInstance = templateInstance._component;
      Klass.prototype[handlerName].call(componentInstance, event, templateInstance, ...extra, this);
    })));
  }

  const helpers = (Klass.HelperMap ? Klass.HelperMap() : {}) || {};
  if (_.isArray(helpers)) {
    registerHelpers(template, Klass, helpers, helpers);
  }
  else {
    registerHelpers(template, Klass, Object.keys(helpers), Object.values(helpers));
  }


  function stateHelper(stateName) {
    const componentInstance = Tracker.nonreactive(() => Template.instance())._component;
    return componentInstance.get(stateName);
  }

  const exposeStates = Klass.ExposeStateMap ? (Klass.ExposeStateMap() || {}) : {};
  const stateNames = _.isArray(exposeStates) ? exposeStates : Object.values(exposeStates);
  const helperNames = _.isArray(exposeStates) ? exposeStates : Object.keys(exposeStates);
  if (helperNames.length) {
    template.helpers(_.object(helperNames, stateNames.map(stateName => {
      const fn = stateHelper.bind(undefined, stateName);
      fn.useNonReactiveData = true;
      return fn;
    })));
  }

  function self(...fields) {
    return Tracker.guard(() => {
      const data = Template.currentData();
      if (!fields.length) {
        return data;
      }
      const ret = {};
      fields.slice(0, -1).forEach((field) => {
        ret[field] = data[field]
      });
      return ret;
    });
  }
  self.useNonReactiveData = true;

  function root(...fields) {
    const componentInstance = Tracker.nonreactive(() => Template.instance())._component;
    return componentInstance.reactiveData(...fields.slice(0, -1));
  }
  root.useNonReactiveData = true;

  template.helpers({
    self,
    root
  });

  const bindUiToState = Klass.BindUIToState ? Klass.BindUIToState() : [];
  bindUiToState.forEach((obj) => {
    template.events({
      [obj.event](e, templInstance) {
        const $target = templInstance.$(e.currentTarget);
        const state = obj.state || $target.attr(obj.stateAttribute);
        let value = obj.valueAttribute ? $target.attr(obj.valueAttribute) : obj.val();
        if (obj.convert && obj.convert instanceof Function) {
          value = obj.convert(value);
        }
        else if ((typeof obj.convert) === 'string') {
          value = templInstance._component[obj.convert].call(templInstance._component, value);
        }
        templInstance._component.set(state, value);
      }
    });
  });

  template.onCreated(function onCreated() {
    if (Klass.Schema) {
      const schema = Klass.Schema();
      const context = schema.namedContext("myContext");
      const isValid = context.validate(this.data);
      if (!isValid) {
        const error = new Meteor.Error(500, `Invalid data\n${context.invalidKeys().join("\n")}`);
        if (process.env.NODE_ENV === "development") {
          throw error;
        }
        else {
          console.error(error);
        }
      }
    }
    this._component = new Klass(this);
  });

  if (Klass.prototype.destructor) {
    template.onDestroyed(function onDestroyed() {
      this._component.destructor();
      delete this._component;
    });
  }

  if (Klass.prototype.rendered) {
    template.onRendered(function onRendered() {
      this._component.rendered();
    });
  }
};
