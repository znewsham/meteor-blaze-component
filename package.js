Package.describe({
  name: 'znewsham:blaze-component',
  version: '2.0.0',
  // Brief, one-line summary of the package.
  summary: 'Provides a helper class to make common patterns with blaze easier.',
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/znewsham/meteor-blaze-component/overview',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Npm.depends({
  "underscore": "1.9.1",
});

Package.onUse(function(api) {
  api.versionsFrom(["METEOR@1.4"]);
  api.use('ecmascript');
  api.use(["reactive-dict", "blaze", "templating", "tracker", "ejson"], "client");
  api.mainModule('blaze-component.js', 'client', { lazy: true });
  api.mainModule('blaze-component.js', 'server', { lazy: true });
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('znewsham:blaze-component');
  api.mainModule('blaze-component-tests.js');
});
